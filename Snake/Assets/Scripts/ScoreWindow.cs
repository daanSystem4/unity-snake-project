
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreWindow : MonoBehaviour
{

    private TMPro.TextMeshProUGUI scoreText;

    private void Awake()
    {
        scoreText = transform.Find("scoreText").GetComponent<TMPro.TextMeshProUGUI>();
    }

    private void Update()
    {
        scoreText.text = GameHandler.GetScore().ToString();
    }
}
