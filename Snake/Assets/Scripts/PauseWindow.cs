using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseWindow : MonoBehaviour {

    private static PauseWindow instance;

    public void resumeBtn(){
        GameHandler.ResumeGame();
        Debug.Log("resume");
    }

    public void mainMenuBtn(){
        Loader.Load(Loader.Scene.MainMenu);
        Debug.Log("Back to Menu");
    }
    
    private void Awake() {
        instance = this;

        transform.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        transform.GetComponent<RectTransform>().sizeDelta = Vector2.zero;

        Hide();
    }

    private void Show() {
        gameObject.SetActive(true);
    }

    private void Hide() {
        gameObject.SetActive(false);
    }

    public static void ShowStatic() {
        instance.Show();
    }

    public static void HideStatic() {
        instance.Hide();
    }
}
